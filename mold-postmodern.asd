;;;; mold.asd

(asdf:defsystem #:mold-postmodern
  :description "Describe mold here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :mold-store :postmodern :cl-json)
  :components ((:file "postmodern")))
