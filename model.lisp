;; When defining a model just with classes, a lot of information is made implicit rather than explicit.
;; Particularly relationships between objects. But also more aspects that are relevant for persistence, serialization, and a lot more.
;; So, the proposal is to encode models and its relationships explicitly, and also provide models for different aspects,
;; so that it is possible to derive different services from the model, like persistence, serialization, validation,
;; HTTP api service, even uml diagram generation, and more.
;; The idea is to have a meta model description language for describing the model and its different aspects, and then
;; implement service generators that use that model for either generating code or interpret it to provide a service.
;; Service generators can also be viewed as integrators between the generic meta model and the particular libraries it works with.
;; Candidate services:
;; - Persistence. Postmodern and other mappings (Mito, CLSQL, simple memory persistence). Provide more ORM capabilities than dao-class.
;; - Serialization. Serialize to json using the meta model and a serialization aspect.
;; - Validation. Validate a model input using the meta model as schema.
;; - REST API generation. Using serialization and persistence, the model can be navigated (as relationships are explicit) and provide an OData REST api.
;; - UML diagram generation. Generate PlantUML definitions from the meta model and an UML aspect.
;; - Web form generation. Generate cl-forms forms from a meta model plus a form aspect.
;; TODO: look at Statice for the DSL terms
;; Nombres:
;; - Mold
;; - CAMEL
;; - Semantic-Model (LISA : Lisp Semantic Model)
;; XMI import/export for leverage of tools (see Eclipse Modeling Tools). For example, use XMI/Lisp extensions for CLOS class definition from XMI (accessor, initarg, etc)

;; Consider: consider having "refinements" instead of "aspects". def-model-refinement?

(in-package :mold)

(defvar *model-packages* (make-hash-table))
(defvar *model-package*)

(defclass model-package ()
  ((name :initarg :name
         :initform (error "Provide the package name")
         :accessor model-package-name)
   (models :initform (make-hash-table)
           :accessor model-package-models)))

(defmethod print-object ((model-package model-package) stream)
  (print-unreadable-object (model-package stream :type t :identity t)
    (princ (model-package-name model-package) stream)))

;; We could implement MODEL as an instance of MODEL-OBJECT, and then we would have a metacircular design.
;; MODEL-OBJECT = id + slots + type. (objects in OOP).
;; MODEL = attributes + supers. (classes in OOP).
;; MODEL extends MODEL-OBJECT (everthing is an object).
;; MODEL-OBJECT is instance of MODEL (every object is instance of a model).

(defclass model ()
  ((name :initform (error "Provide the data model name")
         :accessor model-name
         :initarg :name :type symbol)
   (supermodels :initform nil
                :accessor supermodels
                :initarg :supermodels
                :type list)
   (attributes :initform nil
               :accessor model-attributes
               :initarg :attributes
               :type list)
   (options :initform nil
            :accessor model-options
            :initarg :options)
   (metamodel :initform (error "Provide the metamodel")
              :accessor metamodel
              :initarg :metamodel)
   (package :initarg :package
            :accessor model-package
            :initform *model-package*))
  (:documentation "A MODEL is a description of some object."))

(defclass model-object ()
  ((model :initarg :model
          :accessor model))
  (:documentation "A MODEL-OBJECT is an instance of a MODEL.
Provides access to values of attributes defined in its model."))

(defgeneric attribute-value (model-object attribute-name))
(defgeneric (setf attribute-value) (value model-object attribute-name))
(defgeneric attribute-boundp (model-object attribute-name))

(defclass standard-model-object (model-object)
  ((values :initform (make-hash-table)
	   :accessor model-object-values))
  (:documentation "A standard MODEL-OBJECT"))

(defmethod attribute-value ((model-object standard-model-object) attribute-name)
  (let ((attribute (find-attribute (model model-object) attribute-name)))
    (funcall (or (attribute-reader attribute)
		 (lambda (obj)
		   (gethash (attribute-name attribute) (model-object-values obj))))
	     model-object)))

(defmethod (setf attribute-value) (value (model-object standard-model-object) attribute-name)
  (let ((attribute (find-attribute (model model-object) attribute-name)))
    (funcall (or (attribute-writer attribute)
		 (lambda (val obj)
		   (setf (gethash (attribute-name attribute) (model-object-values obj)) val)))
           value model-object)))

(defclass pluggable-model-object (model-object)
  ((object :initarg :object
           :accessor object))
  (:documentation "A MODEL-OBJECT that uses a STANDARD-OBJECT for storing its attribute values."))


(defmethod attribute-value ((model-object pluggable-model-object) attribute-name)
  (let ((attribute (find-attribute (model model-object) attribute-name)))
    (funcall (or (attribute-reader attribute)
		 (lambda (obj)
		   (slot-value obj (attribute-name attribute))))
	     (object model-object))))

(defmethod (setf attribute-value) (value (model-object pluggable-model-object) attribute-name)
  (let ((attribute (find-attribute (model model-object) attribute-name)))
    (funcall (or (attribute-writer attribute)
		 (lambda (val obj)
		   (setf (slot-value obj (attribute-name attribute)) val)))
	     value (object model-object))))

(defmethod print-object ((model model) stream)
  (print-unreadable-object (model stream :type t :identity t)
    (format stream "~s" (model-name model))))

(defun id-attribute (model)
  (first (option-value model :id-attribute)))

(defun id-slot (model)
  (or (id-attribute model)
      (error ":id-attribute not defined for ~a" model)))

(defclass model-aspect (model)
  ((aspect :initarg :aspect
           :accessor aspect
           :initform (error "Provide the aspect"))))

(defmethod print-object ((model-aspect model-aspect) stream)
  (print-unreadable-object (model-aspect stream :type t :identity t)
    (format stream "~a ~a" (model-name model-aspect) (aspect model-aspect))))

(defun initialize-model-packages ()
  (setf *model-packages* (make-hash-table))
  (make-model-package :default)
  (setf *model-package* (find-model-package :default)))

(defun make-model-package (name)
  (setf (gethash name *model-packages*)
        (make-instance 'model-package :name name)))

(defmacro def-model-package (name)
  `(make-model-package ',name))

(defmacro in-model-package (name)
  `(setf *model-package* (find-model-package ',name)))

(defun find-model-package (package-or-name)
  (if (typep package-or-name 'model-package)
      package-or-name
      (or (gethash package-or-name *model-packages*)
          (error "Model package not defined: ~a" package-or-name))))

(defun find-model (name &optional (package *model-package*))
  (or (gethash name (model-package-models package))
      (error "Model not found: ~a (in ~a package)" name (model-package-name package))))

(defun find-model? (name &optional (package *model-package*))
  (gethash name (model-package-models package)))

(defun find-models (predicate &optional (package *model-package*))
  (loop
    for model being the hash-values of package
    when (funcall predicate model)
      collect model))

(defun list-model-package-models (&optional (package *model-package*))
  (alexandria:hash-table-values (model-package-models (find-model-package package))))

(defun add-model (model package)
  (setf (gethash (model-name model) (model-package-models package)) model))

(defmacro def-model (name supermodels attributes &rest options)
  `(let ((model
           (make-instance 'model
                          :name ',name
                          :supermodels ',supermodels
                          :attributes ',attributes
                          :options ',options
                          :metamodel ',(or (cdr (find :metamodel options :key 'car))
                                           'model))))
     (setf (get ',name :model) model)
     (add-model model *model-package*)))

(defmacro def-model-aspect (aspect name supermodels attributes &rest options)
  `(let ((model
           (make-instance 'model-aspect
                          :name ',name
                          :supermodels ',supermodels
                          :attributes ',attributes
                          :aspect ',aspect
                          :options ',options
                          :metamodel ',(or (cdr (find :metamodel options :key 'car))
                                           aspect))))
     (setf (aget (get ',name :model-aspects) ',aspect)
           model)
     model))

(defun find-model-aspect (model-or-name aspect)
  (let ((model-name (if (symbolp model-or-name)
                        model-or-name
                        (model-name model-or-name))))
    (cdr (assoc aspect (get model-name :model-aspects) :key 'string :test 'string=))))

(defun merge-models (&rest models)
  (reduce 'merge-model models))

(defun merge-plists (plist1 plist2)
  (append plist2
          (loop for key in plist1 by #'cddr
                for value in (cdr plist1) by #'cddr
                when (not (member key plist2))
                  collect key
                when (not (member key plist2))
                  collect value)))

(defun merge-model (m1 m2)
  (make-instance 'model
                 :name (model-name m1)
                 :supermodels (remove-duplicates (append (supermodels m1) (supermodels m2)))
                 :attributes (loop for attribute in (model-attributes m1)
                                   collect (cons (first attribute)
                                                 (merge-plists (cdr attribute)
                                                               (cdr (find (first attribute) (model-attributes m2) :key 'car)))))
                 :options (append (model-options m1) (model-options m2))
                 :metamodel (list (metamodel m1) (metamodel m2))))

;; Attributes

(defun find-attribute (model attribute-name)
  (find attribute-name (model-attributes model) :key 'attribute-name))

(defun find-option (model option)
  (find option (model-options model) :key 'car))

(defun option-value (model option)
  (cdr (find-option model option)))

(defun attribute-name (attribute)
  (first attribute))

(defun attribute-option (option attribute &optional default)
  (getf (cdr attribute) option default))

(defun attribute-options (attribute)
  (cdr attribute))

(defun attribute-accessor (attribute)
  (attribute-option :accessor attribute))

(defun attribute-writer (attribute)
  (or (attribute-option :writer attribute)
      (attribute-accessor attribute)))

(defun attribute-reader (attribute)
  (or (attribute-option :reader attribute)
      (attribute-accessor attribute)))

;; Types

;;(defstruct association
;;  type arity roles)

(defun attribute-type (attribute)
  (attribute-option :type attribute))

(defun is-reference? (attribute)
  (is-reference-type? (attribute-type attribute)))

(defun is-reference-type? (type)
  (and (listp type)
       (string= (string (first type))
                (string 'reference-to))))

(defun is-multiple? (attribute)
  (is-multiple-type? (attribute-type attribute)))

(defun is-multiple-type? (type)
  (and (listp type)
       (member (first type) '(set-of list many member))))

(defun is-association? (attribute)
  (is-association-type? (attribute-type attribute)))

(defun is-association-type? (type)
  (and (listp type)
       (member (string (first type))
               (mapcar 'string '(reference-to set-of))
               :test 'string=)))

(defun association-type (attribute)
  "Return the target type of the association"
  (assert (is-association? attribute))
  (values (second (attribute-type attribute))
          (first (attribute-type attribute))))

(defun is-abstract? (model)
  (option-value model :abstract))

(defun format-symbol (symbol &rest args)
  (read-from-string (apply #'format nil (prin1-to-string symbol) args)))

(defun concat-symbols (&rest args)
  (read-from-string (apply #'concatenate 'string (mapcar 'prin1-to-string args))))

(defmacro def-metamodel (name &key attribute-options model-options initializer))

(defun validate-model (model)
  "Validate model via its metamodel")

(def-metamodel standard-model
  :attribute-options (:type :initarg :initform
                            :accessor :writer :reader
                      :required :documentation)
  :model-options (:metaclass :documentation :abstract)
  :initializer initialize-model)

(def-metamodel <<database>>
  :attribute-options (:db-type :unique :foreign-key
                               :db-mapping :db-ghost :db-default
                      :primary-key)
  :model-options (:table-name :foreign :unique :keys)
  :initializer initialize-<<database>>)

(def-metamodel <<serialization>>
  :attribute-options (:serialize :serializer :unserializer :parser :formatter))

(def-metamodel <<schema>>
  :attribute-options (:schema-type :formatter :parser :validator))

;;;; * Querying

(defun associations (model)
  (remove-if-not 'is-association? (model-attributes model)))

(defun outcoming-associations (model)
  (associations model))

(defun incoming-associations (model)
  (loop for m in (list-model-package-models (model-package model))
        when (not (eql (model-name model) (model-name m)))
          appending (loop for association in (associations m)
                          when (eql (second (attribute-type association))
                                    (model-name model))
                            collect (cons m association))))

(defun direct-connections (model)
  "Return the direct connections model. Connections are either associations or inheritance relationships."
  (append (mapcar (lambda (assoc) (cons :outcoming assoc))
                  (outcoming-associations model))
          (mapcar (lambda (assoc) (cons :incoming assoc))
                  (incoming-associations model))
          (mapcar (lambda (model) (cons :supermodel model))
                  (supermodels model))))

(defun submodels (model)
  "A model direct submodels"
  (let ((model-package (model-package model)))
    (remove-if-not (lambda (m)
                     (member (model-name model) (supermodels m)))
                   (list-model-package-models model-package))))

;; Class definition functions

(defun attribute->slot-def (attribute-def)
  (flet ((convert-type (type-def)
           (if (listp type-def)
               (case (first type-def)
                 (reference nil)
                 (t nil))
               (case type-def
                 (mold::datetime 'string)
                 (mold::time 'string)
                 (t type-def)))))
    (let ((slot-def (copy-list attribute-def))
          (slot-type (convert-type (getf (cdr attribute-def) :type))))
      (if (null slot-type)
          (remf (cdr slot-def) :type)
          (setf (getf (cdr slot-def) :type) slot-type))
      slot-def)))

(defgeneric get-class-model (class))

(defmacro def-class-from-model (name)
  (let* ((model (find-model name)))
    `(progn
       (defclass ,(model-name model) ,(supermodels model)
         ,(mapcar 'attribute->slot-def (model-attributes model))
         ,@(remove-if-not
            (lambda (option)
              (member option '(:default-initargs :documentation :metaclass)))
            (model-options model)
            :key 'car))
       (defmethod get-class-model ((class (eql ',name)))
         (find-model ',name))
       #+nil(defmethod print-object ((object ',name) stream)
         (print-unreadable-object (object stream :type t :identity t)
           (format stream "~a" (slot-value object 'id))))
       )))

(defmacro def-classes-from-package (package)
  `(progn
     ,@(loop for model in (mold:list-model-package-models package)
             collect `(def-class-from-model ,(model-name model)))))

;; Initialization

(initialize-model-packages)
