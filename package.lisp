;;;; package.lisp

(defpackage #:mold
  (:use #:cl)
  (:use :assoc-utils :anaphora)
  (:export
   #:def-model-package
   #:in-model-package
   #:def-model
   #:def-model-aspect

   #:find-model-package
   #:find-model
   #:find-model?
   #:find-models
   #:find-model-aspect
   #:model-package-models
   #:list-model-package-models
   #:model-package

   #:get-class-model
   
   #:model
   ;;#:metamodel
   #:def-metamodel

   #:merge-models
   #:merge-model
   
   #:find-option
   #:option-value

   #:model-name
   #:model-attributes
   #:supermodels
   #:model-options
   #:find-attribute

   #:id-attribute
   #:id-slot

   #:attribute-name
   #:attribute-option
   #:attribute-options
   #:attribute-type
   #:attribute-reader
   #:attribute-writer
   #:attribute-accessor
   #:is-multiple?
   #:is-association?
   #:is-abstract?
   #:is-association-type?
   #:is-multiple-type?
   #:is-reference?
   #:association-type

   #:associations
   #:direct-connections
   #:outcoming-associations
   #:incoming-associations
   #:submodels

   #:def-class-from-model
   #:def-classes-from-package

   #:model-object
   #:pluggable-model-object
   #:attribute-value
   #:attribute-boundp
   
   ;; types
   #:set-of
   #:reference-to
   #:datetime
   #:time
   #:date
   ))
