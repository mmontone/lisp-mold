(defpackage :mold/xmi
  (:use :cl :mold :cxml)
  (:documentation "Read models from XMI"))

(in-package :mold/xmi)

(defmacro define-models-from-xmi (pathname &rest options))
