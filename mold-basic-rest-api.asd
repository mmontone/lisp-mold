(asdf:defsystem #:mold-basic-rest-api
  :description "Describe mold here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :mold-store :mold-serializer :mold-web :cl-json)
  :components ((:file "basic-rest-api")))
