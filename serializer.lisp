(defpackage :mold/serializer
  (:use :cl :mold)
  (:export :serialize))

(in-package :mold/serializer)

(defgeneric serialize (format model-object stream &rest options))

(defmethod serialize ((format (eql :json)) (model-object model-object) stream &rest options)
  (let* ((model (mold/store::model model-object))
         (serialization-aspect (find-model-aspect (model-name model) :<<serialization>>))
         (serialization-model (or (and serialization-aspect (merge-models model serialization-aspect))
                                  model)))
    (json:with-object (stream)
      (loop for attribute in (model-attributes serialization-model)
            unless (not (attribute-option :serialize attribute :not-present))
              unless (is-association? attribute)
                do
                   (json:as-object-member ((or (attribute-option :serialized-name attribute)
                                               (attribute-name attribute)) stream)
                     (serialize-attribute format
                                          (mold/store:attribute-value model-object (attribute-name attribute))
                                          attribute stream))))))

(defmethod serialize ((format (eql :json)) (collection mold/store:model-collection) stream &rest options)
  (json:with-array (stream)
    (loop for element in (mold/store:collection-contents collection)
          do (json:as-array-member (stream)
               (serialize format element stream)))))

(defgeneric serialize-attribute (format value attribute stream))

(defmethod serialize-attribute ((format (eql :json)) value attribute stream)
  (let ((attribute-serializer (or (attribute-option :serializer attribute)
                                  (attribute-type-serializer format (attribute-type attribute)))))
    (funcall attribute-serializer value attribute stream)))

(defgeneric attribute-type-serializer (format attribute-type))

(defmethod attribute-type-serializer ((format (eql :json)) attribute-type)
  (lambda (value attribute stream)
    (declare (ignore attribute))
    (json:encode-json value stream)))

(defmethod attribute-type-serializer ((format (eql :json)) (attribute-type (eql 'boolean)))
  (lambda (value attribute stream)
    (declare (ignore attribute))
    (if value
        (format stream "true")
        (format stream "false"))))

(defmethod attribute-type-serializer ((format (eql :json)) (attribute-type (eql 'boolean)))
  (lambda (value attribute stream)
    (declare (ignore attribute))
    (if value
        (format stream "true")
        (format stream "false"))))
