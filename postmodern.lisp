(defpackage :mold/postmodern
  (:use :cl :postmodern :mold)
  (:export #:def-dao-from-model
           #:def-orm-dao-from-model
           #:def-daos-from-package
           #:def-orm-daos-from-package))

(in-package :mold/postmodern)

(defmacro def-daos-from-package (package)
  `(progn
     ,@(loop for model in (mold:list-model-package-models (mold:find-model-package package))
             collect `(def-dao-from-model ,(model-name model)))))

(defmacro def-orm-daos-from-package (package)
  `(progn
     ,@(loop for model in (mold:list-model-package-models package)
             collect `(def-orm-dao-from-model ,(model-name model)))))

(defmacro def-json-accessor (accessor class slot)
  `(progn
     (defmethod ,accessor ((object ,class))
       (json:decode-json-from-string (slot-value object ',slot)))
     (defmethod (setf ,accessor) (value (object ,class))
       (setf (slot-value object ',slot) (json:encode-json-to-string value)))))

(defmacro def-dao-from-model (name)
  (let* ((model (find-model name))
         (db-aspect (find-model-aspect name :<<database>>))
         (db-dm (merge-models model db-aspect)))
    `(progn
       (defclass ,(model-name db-dm) ,(supermodels db-dm)
         ,(loop for attr in (model-attributes db-dm)
                when (attribute-option :db-field attr)
                  do (warn ":db-field option not supported by postmodern:dao-class")
                when (not (is-association? attr))
                  collect (append attr `(:col-type ,(or (attribute-option :db-type attr)
                                                        (attribute-option :type attr))
                                         :col-default ,(attribute-option :db-default attr)
                                         :ghost ,(attribute-option :db-ghost attr)))
                ;; Create database fields for references
                when (is-reference? attr)
                  collect (if (attribute-option :db-mapping attr)
                              ;; look at how to do the mapping
                              (error "TODO: don't know how to map :db-mapping ~s" attr)
                              ;; else, automatic mapping, create an <attr name>-id integer field
                              (let ((field-name (intern (format nil "~a-ID" (attribute-name attr)))))
                                `(,field-name :col-type integer :type integer :accessor ,field-name :initarg ,(alexandria:make-keyword (string field-name))))))
         (:metaclass postmodern:dao-class)
         (:table-name ,(table-name db-dm))
         (:keys ,@(loop for db-attr in (model-attributes db-aspect)
                        when (getf (cdr db-attr) :primary-key)
                          collect (first db-attr))))
       (postmodern:deftable ,(model-name db-dm)
         (postmodern:dao-table-definition ',(model-name db-dm))
         ,@(let ((uniques (loop for db-attr in (model-attributes db-aspect)
                                when (attribute-option :unique db-attr)
                                  collect (attribute-name db-attr))))
             (and uniques `((postmodern:!unique ',uniques))))
         ,@(loop for db-attr in (model-attributes db-aspect)
                 when (attribute-option :foreign-key db-attr)
                   collect `(postmodern:!foreign ,@(attribute-option :foreign-key db-attr)))
         ,@(when (option-value db-aspect :unique)
             (list `(!unique ',(option-value db-aspect :unique))))
         ,@(when (option-value db-aspect :foreign)
             (list `(!foreign ',(option-value db-aspect :foreign)))))

       ;; Define special accessors
       ,@(loop for attr in (model-attributes db-dm)
               when (string= (princ-to-string (attribute-option :db-type attr))
                             "JSON")
                 collect
               `(def-json-accessor ,(or (attribute-option :accessor attr)
                                        (attribute-name attr))
                    ,(model-name model)
                  ,(attribute-name attr)))
       )))

(defmacro def-orm-dao-from-model (name)
  (let* ((model (find-model name))
         (db-aspect (find-model-aspect name '<<database>>))
         (db-dm (merge-models model db-aspect)))
    `(progn
       (def-dao-from-model ,name)
       (def-relationships ,name
         ,@(loop for attr in (remove-if-not 'is-association? (model-attributes db-dm))
                 when (attribute-option :db-mapping attr)
                   collect (attribute-option :db-mapping attr)))
       )))

(defun build-query (&key (select '*) from (where t) order-by limit offset)
  (let ((q `(:select ,select
              :from ,@from
              :where ,where)))
    (when order-by
      (setf q `(:order-by ,q ,order-by)))
    (when limit
      (setf q `(:limit ,q ,limit ,offset)))
    q))

(defun compile-query  (query &key count limit offset where order-by)
  (let ((q query))
    (when order-by
      (setf q `(:order-by ,q ,order-by)))
    (when limit
      (setf q `(:limit ,q ,limit ,offset)))
    q))

(defmacro def-query-method (name args dao-type query)
  (let ((count-query (sql-compile (let ((countq (copy-list query)))
                                    (setf (nth 1 countq) (list :count '*))
                                    countq))))
    (alexandria:with-gensyms (q)
      `(defmethod ,name (,@args &key count limit offset order-by where)
         (let ((,q (list ,@query)))
           (if count
               (query ,count-query :single)
               (query-dao ',dao-type
                          (sql-compile
                           (compile-query ,q
                                          :limit limit
                                          :offset offset
                                          :where where
                                          :order-by order-by)))))))))

(defmacro def-one-to-one (class field-name target-class &key (id-field :id) reader)
  `(defmethod ,(or reader target-class) ((obj ,class))
     (let ((val (slot-value obj ',field-name)))
       (when (not (eql val :null))
         (get-dao ',target-class ,id-field val)))))

(defmacro def-one-to-ones (class &body defs)
  `(progn
     ,@(loop for def in defs
             collect `(def-one-to-one ,class ,@def))))

(defmacro def-one-to-many (one-class name many-class many-field)
  (flet ((field (table field-name)
           (make-symbol (string-upcase (format nil "~a.~a" table field-name)))))
    `(defmethod ,name ((obj ,one-class) &key  order-by limit (offset 0) where count)
       (let ((select (if count
                         `(count (quote ,',(field (dao-table-name many-class) '*)))
                         (quote ,(field (dao-table-name many-class) '*)))))
         (let ((query `(:select ,select
                         :from ,',(dao-table-name many-class)
                         :where (:and (:= ,(id obj)
                                          ,',(field (dao-table-name many-class) many-field))
                                      ,@where))))
           (when (and (not count) order-by)
             (setf query `(:order-by ,query ,order-by)))
           (when (and (not count) limit)
             (setf query `(:limit ,query ,limit ,offset)))
           (if count
               (query (sql-compile query) :single)
               (query-dao ',many-class (sql-compile query))))))))

(defmacro def-many-to-many (one-class name many-class through-class &optional back-field target-field)
  (let ((back-field (or back-field (intern (concatenate 'string (string one-class) "-ID"))))
        (target-field (or target-field (intern (concatenate 'string (string many-class) "-ID"))))
        (many-table (or (and (find-class many-class)
                             (dao-table-name many-class))
                        many-class))
        (through-table (or (and (find-class through-class)
                                (dao-table-name through-class))
                           through-class)))
    (flet ((field (table field-name)
             (make-symbol (string-upcase (format nil "~a.~a" table field-name)))))
      `(defmethod ,name ((obj ,one-class) &key  order-by limit (offset 0) where count)
         (let ((select (if count
                           `(count (quote ,',(field many-table '*)))
                           (quote ,(field many-table '*)))))
           (let ((query `(:select ,select
                           :from ,',many-table ,',through-table
                           :where (:and
                                   (:= ,',(field through-table back-field)
                                       ,(id obj))
                                   (:= ,',(field through-table target-field)
                                       ,',(field many-table 'id))
                                   ,@where))))
             (when (and (not count) order-by)
               (setf query `(:order-by ,query ,order-by)))
             (when (and (not count) limit)
               (setf query `(:limit ,query ,limit ,offset)))
             (if count
                 (query (sql-compile query) :single)
                 (query-dao ',many-class (sql-compile query)))))))))

(defmacro def-relationships (class &body defs)
  `(progn
     ,@(loop for def in defs
             collect
             (ecase (first def)
               (:one-to-one `(def-one-to-one ,class ,@(rest def)))
               (:one-to-many `(def-one-to-many ,class ,@(rest def)))
               (:many-to-many `(def-many-to-many ,class ,@(rest def)))))))

(defun table-name (model)
  (or (first (option-value model :table-name))
      (string-downcase (string (model-name model)))))

(defun list-models-tables (package)
  (loop for model in (list-model-package-models package)
        when (not (is-abstract? model))
          collect (table-name model)))

(defun create-model-package-schema (name)
  (with-schema (name :if-not-exist :create :drop-after nil)
    (loop for table in (list-models-tables mold::*model-package*)
          do
             (create-table table))))

;; Store

;; TODO: consider sxql-composer library for query building here.

(defclass postmodern-model-store (mold/store::model-store)
  ((database-connection :accessor database-connection)))

(defclass postmodern-model-collection (mold/store:model-collection)
  ((insert-function :initarg :insert-function :accessor insert-function)
   (query :initarg :query :accessor collection-query)))

(defmethod mold/store:contents ((collection postmodern-model-collection) &rest args)
  (let ((query (apply (collection-query collection) args)))
    (postmodern:query-dao (mold/store:collection-type collection)
                          query)))

(defmethod store-get-collection ((store postmodern-model-store) model-type)
  (let ((model (find-model model-type (model-package store))))
    (make-instance 'postmodern-model-collection
                   :store store
                   :collection-type model
                   :query (lambda (&rest args)
                            `(:select * :from ,(s-sql:to-sql-name model-type)))
                   :insert-function (lambda (dao)
                                      ;; No attribute assignment for root collection
                                      ;; TODO: check dao has correct type
                                      (postmodern:save-dao dao)))))

(defmethod collection-find ((collection postmodern-model-store) id)
  (postmodern:get-dao (mold/store:collection-type collection)
                      id))

(defmethod collection-add ((collection postmodern-model-collection) object)
  (funcall (insert-function collection) object))

(defmethod store-get-attribute-value ((store postmodern-model-store) model-object attribute)
  (flet ((query-objects (collection-type)
           ;; TODO: figure out the db mapping here
           (let* ((back-field (attribute-name attribute))
                  (id-slot (id-slot (mold/store::model model-object)))
                  (object-id (slot-value (mold/store::object model-object) id-slot)))
             (postmodern:query-dao collection-type
                                   `(:select * :from ,(s-sql:to-sql-name (model-name collection-type))
                                      :where (:= ,back-field ,object-id))))))
    (cond
      ((is-association? attribute)
       (if (is-multiple? attribute)
           ;; return a collection
           (let ((collection-type (find-model (second (attribute-type attribute)))))
             (make-instance 'postmodern-model-collection
                            :store store
                            :collection-type collection-type
                            :contents (lambda ()
                                        (mapcar (lambda (el)
                                                  (mold/store:make-store-object store el))
                                                (query-objects collection-type)))
                            :insert-function (lambda (obj)
                                               ;; TODO: figure out the db mapping here
                                               (let* ((back-slot (attribute-name attribute))
                                                      (id-slot (id-slot (mold/store::model model-object)))
                                                      (object-id (slot-value (mold/store::object model-object) id-slot)))
                                                 (setf (slot-value obj back-slot) object-id)
                                                 (save-dao obj)))))

           ;; else
           ;; just return the referenced object
           (slot-value (mold/store::object model-object)
                       (attribute-name attribute))))
      (t (slot-value (mold/store::object model-object)
                     (attribute-name attribute))))))
