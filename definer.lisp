(defpackage :definer
  (:use :cl)
  (:export
   #:def
   #:expand-definition
   #:find-definition
   #:apropos-definition
   #:definition-documentation
   #:describe-definition))

(in-package :definer)

(defvar *definition-types* '(var parameter function class macro))
(defvar *definition-type-dispatchers*
  '((find-class . define-from-class))
  "An association list with elements with the form: (<predicate> . <definer-function>).")

(defmacro def (definition-type name &rest args)
  (expand-definition definition-type name args))

(defgeneric expand-definition (definition-type name body))

(defmethod expand-definition ((definition-type symbol) name body)
  "The default implementation goes through *DEFINITION-TYPE-DISPATCHERS*.
Tries to find a definer function for DEFINITION-TYPE symbol."
  (dolist (dispatcher *definition-type-dispatchers*)
    (destructuring-bind (predicate . func) dispatcher
      (when (funcall predicate definition-type)
        (return-from expand-definition
          (funcall func definition-type name body)))))
  (call-next-method))

(defmethod expand-definition ((definition-type (eql 'var)) varname source)
  `(defvar ,varname ,@source))

(defmethod expand-definition ((definition-type (eql 'parameter)) varname source)
  `(defparameter ,varname ,@source))

(defmethod expand-definition ((definition-type (eql 'function)) fname body)
  (destructuring-bind ((&rest fargs) fbody)
      body
    `(defun ,fname ,fargs
       ,fbody)))

(defmethod expand-definition ((definition-type (eql 'fun)) fname body)
  (destructuring-bind ((&rest fargs) fbody)
      body
    `(defun ,fname ,fargs
       ,fbody)))

(defmethod expand-definition ((definition-type (eql 'macro)) mname body)
  (destructuring-bind ((&rest margs) mbody)
      body
    `(defmacro ,mname ,margs
       ,mbody)))

(defmethod expand-definition ((definition-type (eql 'class)) class-name body)
  `(defclass ,class-name ,@body))

(defmethod expand-definition ((definition-type (eql 'generic)) fname spec)
  `(defgeneric ,fname ,@spec))

(defmethod expand-definition ((definition-type (eql 'method)) fname spec)
  `(defmethod ,fname ,@spec))

(defun define-from-class (class-name name body)
  (destructuring-bind ((&rest supers)
                       (&rest slots)
                       &rest options)
      body
    `(defclass ,name ,supers
       ,slots
       ,@options
       (:metaclass ,class-name))))

(defgeneric find-definition (name definition-type &key error-p))

(defmethod find-definition (name (definition-type (eql 'function)) &key error-p)
  (symbol-function name))

(defmethod find-definition (name (definition-type (eql 'class)) &key error-p)
  (find-class name))

(defgeneric list-definitions (definition-type))
(defun apropos-definitions (name &key definition-type package external-only))

(defgeneric definition-type-doc-type (definition-type))
(defmethod definition-type-doc-type (definition-type)
  t)

(defmethod definition-type-doc-type ((definition-type (eql 'function)))
  'function)

(defmethod definition-type-doc-type ((definition-type (eql 'class)))
  'type)

(defgeneric definition-documentation (name definition-type))
(defmethod definition-documentation (name definition-type)
  (documentation name (definition-type-doc-type definition-type)))

(defgeneric describe-definition (name definition-type &optional stream))

(defmethod describe-definition (name definition-type &optional (stream *standard-output*))
  (describe (find-definition name definition-type) stream))

;; (def class my-class ()
;;   ((x :initarg :x)))

;; (def fun my-func (x)
;;   (print x))

;; (def var *my-var* 22)

;; (find-definition 'my-func 'function)
;; (find-definition 'my-class 'class)

;; (describe-definition 'my-func 'function)

;; (def generic my-generic (x y))
;; (def method my-generic (x y))

;; (def standard-class my-class ()
;;   ((x :initarg :x)))

(provide :definer)
