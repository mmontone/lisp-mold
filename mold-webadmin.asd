(asdf:defsystem #:mold-webadmin
  :description "A web admin for Mold models"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :mold-webforms :hunchentoot :cl-who)
  :components ((:file "webadmin")))
