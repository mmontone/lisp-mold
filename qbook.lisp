(defpackage :mold/qbook
  (:use :cl :mold :yaclml :arnesi)
  (:documentation "QBook extensions for documenting Mold models"))

(in-package :mold/qbook)

(defparameter *generate-uml* nil)

(defclass model-descriptor (qbook::descriptor)
  ((attributes :accessor attributes :initarg :attributes :initform '())
   (supers :accessor supers :initarg :supers :initform '()))
  (:default-initargs
   :label-prefix "model"
   :pretty-label-prefix "Model"))

(defclass model-attribute-descriptor (qbook::descriptor)
  ((type :initarg :type :accessor attr-type)))

(defun make-attribute-descriptor (attribute-spec)
  (destructuring-bind (name &rest options)
      attribute-spec
    (make-instance 'model-attribute-descriptor
                   :name name
                   :type (getf options :type)
                   :docstring (getf options :documentation))))

(qbook::defcode-info-collector mold:def-model (name supers attributes &rest options)
  (make-instance 'model-descriptor
                 :name name
                 :supers supers
                 :attributes (mapcar #'make-attribute-descriptor attributes)
                 :docstring (second (assoc :documentation options))))

(defmethod qbook::write-code-descriptor ((descriptor model-descriptor) part (generator qbook::html-generator))
  (declare (ignore part))
  (flet ((make-model-link (model)
           (aif (qbook::find-descriptor "model" (model-name model))
                (<:a :href (strcat "../" (qbook::make-anchor-link it))
                     (<:as-html (model-name model)))
                (<:as-html (model-name model)))))
    
    (let ((model (find-model (name descriptor))))
      
      (when (attributes descriptor)
        (<:h2 "Attributes")
        (<:ul
         (dolist (attribute (attributes descriptor))
           (<:li (<:as-html (qbook::name attribute)
                            "::"
                            (attr-type attribute))
                 (when (qbook::docstring attribute)
                   (<:as-html " - " (qbook::docstring attribute)))))))
      (<:h2 "Hierachy")
      (<:h3 "Precedence List")
      (<:ul
       (dolist (name (supermodels model))
         (<:li (make-model-link (find-model name)))))
      (awhen (submodels model)
        (<:h3 "Sub models")
        (<:ul
         (dolist (sub it)
           (<:li (make-model-link sub)))))
      (<:h2 "Associations")
      (<:ul
       (loop for attribute in (model-attributes model)
             when (is-association? attribute)
               do
                  (let ((attribute-type (attribute-type attribute)))
                    (<:li (<:as-is (attribute-name attribute)
                                   ":&nbsp;"
                                   (first attribute-type)
                                   "&nbsp;")
                          (make-model-link (find-model (second attribute-type)))))))
      (when *generate-uml*
        (generate-model-uml model generator)
        (<:h2 "Diagram")
        (<:img :src (format nil "~a-MODEL.png" (model-name model)))))))

(defparameter *plantuml-compiler* "plantuml")

(defun generate-model-uml (model generator)
  (let ((pathname (merge-pathnames (format nil "api/~a-MODEL.puml" (mold:model-name model))
                                   (qbook::output-directory generator))))
    (with-open-file (stream pathname
                          :direction :output
                          :if-exists :supersede
                          :if-does-not-exist :create
                          :external-format :utf8)
    (fmt:fmt stream "@startuml" #\newline #\newline)
    (mold/plantuml:render-model-class-diagram model :stream stream)
    (fmt:fmt stream #\newline "@enduml"))
    (let ((command (format nil "~a ~a"
                           *plantuml-compiler*
                           pathname)))
      (uiop/run-program:run-program command))))
