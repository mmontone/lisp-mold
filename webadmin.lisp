(defpackage :mold/webadmin
  (:use :mold :cl)
  (:export :present-model-object
	   :start-web-admin))

(in-package :mold/webadmin)

(defvar *web-admin-acceptor*)

(defclass web-router-acceptor (hunchentoot:acceptor)
  ((router :initarg :router
	   :accessor router)
   (model-package :initarg :model-package
		  :accessor model-package)))

(defun start-webadmin (model-package-name)
  (let ((model-package (mold:find-model-package model-package-name)))
    (setf *web-admin-acceptor*
	  (make-instance 'web-router-acceptor
			 :port 8082
			 :router (make-instance 'mold/web:simple-model-web-router
						:model-package model-package)
			 :model-package model-package
			 ))
    (hunchentoot:start *web-admin-acceptor*)))

(defmethod hunchentoot:acceptor-dispatch-request
    ((acceptor web-router-acceptor) request)
  (multiple-value-bind (navigation action status)
      (mold/web:route-request (router acceptor)
                              (hunchentoot:request-uri request)
                              (alexandria:make-keyword (hunchentoot:request-method request)))
    (declare (ignore status))
    (let (model)
      (loop for part in navigation
            do
               (trivia:match part
                 ((list :collection collection-type)
                  (setf model (mold/store:get-collection (mold:model-name collection-type))))
                 ((list :find id)
                  (setf model
                        (or (mold/store:collection-find model id)
                            (anaphora:aand (parse-integer id :junk-allowed t)
                                           (mold/store:collection-find model anaphora:it))
                            (error "Model not found: ~a ~a" model id))))
                 (_ (error "Routing error"))))
      (if (null navigation)
          ;; root
          (web/home-page acceptor)
          ;; else
          (cond
            ((typep model 'mold/store:model-collection)
             (ecase action
               (:new (web/new-collection-element model request))
               (:list (web/view-collection model request))
               (:view (web/view-collection model request))
               (:add (web/add-collection-element model request))))
            ((typep model 'mold:model-object)
             (ecase action
               (:view (web/view-model model request))
               (:update (web/update-model model request))))
            (t (error "Type error")))))))

(defun web/home-page (acceptor)
  (who:with-html-output-to-string (html)
    (:h1 (who:fmt "Welcome to web admin"))
    (:ul
     (loop for model in (list-model-package-models (model-package acceptor))
           do (who:htm
               (:li (:a :href (format nil "~a" (model-name model))
                        (who:fmt "~a" (model-name model)))))))))

(defun web/view-collection (collection request)
  (who:with-html-output-to-string (html)
    (:h1 (who:fmt "View collection ~a" (mold:model-name (mold/store:collection-type collection))))
    (:ul
     (loop for model-object in (mold/store:collection-contents collection)
           for object := (mold::object model-object)
           for model := (mold:model model-object)
           do
              (who:htm
               (:li
                (:a :href (format nil "~a/~a"
                                  (hunchentoot:request-uri request)
                                  (slot-value object (mold:id-attribute model)))
                    (who:str (slot-value object (first (option-value model :model-name))))
                    )))))
    (:p (:a :href (format nil "~a/new" (hunchentoot:request-uri request))
            "Add"))))

(defun web/new-collection-element (collection request)
  (who:with-html-output-to-string (forms.who:*html*)
    (:h1 (who:fmt "New element in collection ~a" (mold:model-name (mold/store:collection-type collection))))
    (let ((form (forms:get-form (mold:model-name (mold/store:collection-type collection)))))
      (forms:with-form-renderer :who
        (forms:render-form form)))))

(defun web/add-collection-element (collection request)
  (let* ((object (make-instance (model-name (mold/store:collection-type collection))))
         (model-object (mold/store:make-store-object
                        (mold/store:store collection)
                        object
                        (mold/store:collection-type collection)))
         (form (forms:get-form (model-name (mold/store:collection-type collection))
                               :model object)))
    (forms:handle-request form)
    (mold/store:collection-add collection object)
    (web/view-model model-object request)))

(defun web/view-model (model-object request)
  (present-model-object (mold::object model-object)
			model-object))

(defgeneric present-model-object (object model-object))

(defmethod present-model-object (object model-object)
  (who:with-html-output-to-string (html)
    (:table
     (loop for attribute in (model-attributes (mold/store::model model-object))
           do
              (who:htm (:tr
                        (:td (:b (who:str (attribute-name attribute))))
                        (:td (who:fmt "~a" (mold/store:attribute-value model-object (attribute-name attribute))))))))))
