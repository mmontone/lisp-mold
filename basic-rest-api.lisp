(defpackage :basic-rest-api
  (:use :cl :mold)
  (:export :start-rest-api))

(in-package :basic-rest-api)

(defvar *rest-api-acceptor*)

(defclass rest-api-router-acceptor (hunchentoot:acceptor)
  ((model-package :accessor model-package
		  :initarg :model-package
		  :initform (error "Provide the model package"))
   (router :initarg :router
	   :accessor router)))

(defmethod hunchentoot:acceptor-dispatch-request
    ((acceptor rest-api-router-acceptor) request)
  (multiple-value-bind (navigation action status)
      (mold/web:route-request (router acceptor)
                              (hunchentoot:request-uri request)
                              (alexandria:make-keyword (hunchentoot:request-method request)))
    (declare (ignore status))
    (let (model)
      (loop for part in navigation
            do
               (trivia:match part
                 ((list :collection collection-type)
                  (setf model (mold/store:get-collection (mold:model-name collection-type))))
                 ((list :find id)
                  (setf model
                        (or (mold/store:collection-find model id)
                            (anaphora:aand (parse-integer id :junk-allowed t)
                                           (mold/store:collection-find model anaphora:it))
                            (error "Model not found: ~a ~a" model id))))
                 (_ (error "Routing error"))))
      (if (null navigation)
          ;; root
          (rest-api/root acceptor)
          ;; else
          (cond
            ((typep model 'mold/store:model-collection)
             (ecase action
               (:new (rest-api/new-collection-element model request))
               (:list (rest-api/fetch-collection model request))
               (:view (rest-api/fetch-collection model request))
               (:add (rest-api/add-collection-element model request))))
            ((typep model 'mold/store:store-object)
             (ecase action
               (:view (rest-api/fetch-model model request))
               (:update (rest-api/update-model model request))))
            (t (error "Type error")))))))

(defun rest-api/root (acceptor)
  (who:with-html-output-to-string (html)
    (:h1 (who:fmt "Welcome to web admin"))
    (:ul
     (loop for model in (list-model-package-models (model-package acceptor))
           do (who:htm
               (:li (:a :href (format nil "~a" (model-name model))
                        (who:fmt "~a" (model-name model)))))))))

(defmethod rest-api/fetch-collection (collection request)
  (with-output-to-string (s)
    (mold/serializer:serialize :json collection s)))

(defmethod rest-api/fetch-model (model-object request)
  (with-output-to-string (s)
    (mold/serializer:serialize :json model-object s)))

(defun start-rest-api (model-package-name)
  (let ((model-package (find-model-package model-package-name)))
    (setf *rest-api-acceptor* (make-instance 'rest-api-router-acceptor
					     :port 8083
					     :model-package model-package
					     :router (make-instance 'mold/web:rest-api-model-router
								    :model-package model-package)))
    (hunchentoot:start *rest-api-acceptor*)))
