(defpackage :mold/plantuml
  (:use :cl :mold)
  (:export #:render-class-diagram
           #:render-model-class-diagram))

(in-package :mold/plantuml)

(defun render-class-diagram (&optional (stream *standard-output*) (model-package mold::*model-package*))
  "Render an UML class diagram with all the models of MODEL-PACKAGE"
  (fmt:fmt stream "@startuml" #\newline #\newline)
  (loop for model in (mold:list-model-package-models model-package)
        do
           (render-model-class-diagram model :stream stream))
  (fmt:fmt stream "@enduml"))

(defun uml-type-name (name)
  (string-capitalize (json:lisp-to-camel-case (princ-to-string name))))

(defun uml-name (name)
  (json:lisp-to-camel-case (string name)))

(defun render-model-class-diagram (model &key
                                           (stream *standard-output*)
                                           (include-associations t)
                                           (include-inheritance t))
  "Render the UML class diagram for a model, and its associations"
                                         
  (fmt:fmt stream
           "class " (uml-type-name (model-name model)) " { " #\newline)

  ;; attributes
  (loop for attribute in (model-attributes model)
        when (not (is-association? attribute))
          do (fmt:fmt stream "    "
                      (uml-type-name (attribute-type attribute))
                      #\space
                      (uml-name (attribute-name attribute))
                      #\newline))
  (fmt:fmt stream "}" #\newline #\newline)

  ;; associations
  (when include-associations
    (loop for attribute in (model-attributes model)
          when (is-association? attribute)
            do
               (fmt:fmt stream
                        (uml-type-name (model-name model))
                        " --> "
                        (uml-type-name (second (attribute-type attribute)))
                        " : "
                        "\""
                        (uml-name (attribute-name attribute))
                        "\""
                        #\newline)))

  ;; inheritance
  (when include-inheritance
    (loop for supermodel in (supermodels model)
          do
             (fmt:fmt stream
                      (uml-type-name supermodel)
                      " <|-- "
                      (uml-type-name (model-name model))
                      #\newline)
          ))
  )
