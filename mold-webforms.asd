(asdf:defsystem #:mold-webforms
  :description "Create web forms from models using cl-forms library"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :cl-forms)
  :components ((:file "mold-webforms")))
