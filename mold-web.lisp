(defpackage :mold/web
  (:use :cl :mold :anaphora)
  (:export
   :route-request
   :model-web-router
   :simple-model-web-router
   :rest-api-model-router
   ))

(in-package :mold/web)

(defclass model-web-router ()
  ((handlers :initarg :handlers :accessor handlers)
   (model-package :initarg :model-package
                  :accessor model-package
                  :initform (error "Provide :model-package"))))

(defclass simple-model-web-router (model-web-router)
  ())

(defclass rest-api-model-router (model-web-router)
  ())

(defgeneric route-request (router uri request-method))

(defun find-model-by-name (string model-package &key (error-p t))
  (or (find string (list-model-package-models model-package)
            :key (alexandria:compose 'princ-to-string
                                     'model-name)
            :test 'equalp)
      (and error-p (error "Model not found: ~a" string))))

;; The request routing state machine
(defmethod route-request ((router simple-model-web-router) uri request-method)
  (let ((status :start)
        (path (split-sequence:split-sequence #\/ uri :remove-empty-subseqs t))
        (navigation nil)
        (model-package (model-package router))
        (action nil))
    
    (flet ((route-request (path-part)
             (trivia:match status
               (:start
                (aif (find-model-by-name path-part model-package :error-p nil)
                     (progn
                       (setf status (list :collection it))
                       (push (list :collection it) navigation))
                     (setf status (list :error :collection-not-found path-part))))
               ((list :collection model)
                (cond
                  ((string= path-part "new")
                   (case request-method
                     (:get
                      (setf action :new)
                      (setf status :end))
                     (:post
                      (setf action :add)
                      (setf status :end))
                     (t (setf status (list :error "Bad request")))))
                  (t ;; We assume it is an identifier of a model in the collection
                   (push (list :find path-part) navigation)
                   (setf status (list :model model path-part)))))
               ((list :model model id)
                (cond
                  ((string= path-part "edit")
                   (case request-method
                     (:get (setf action :edit)
                      (setf status :end))))
                  (t ;; We assume this is an attribute access
                   (let ((attribute
                           (find path-part (model-attributes model)
                                 :key (alexandria:compose 'princ-to-string 'attribute-name)
                                 :test 'equalp)))
                     (if (null attribute)
                         (setf status (list :error :attribute-not-found path-part model))
                         (progn
                           (push (list :attribute (attribute-name attribute)) navigation)
                           (if (is-association? attribute)
                               ;; navigate to the associated model/collection
                               (if (is-multiple? attribute)
                                   (setf status (list :collection (find-model (association-type attribute) model-package)))
                                   ;; else
                                   (setf status (list :model (find-model (association-type attribute) model-package) nil)))
                               ;; else
                               (setf status (list :attribute (attribute-name attribute))))))))))
               
               ((list :attribute attribute-name)
                (cond
                  ((string= path-part "edit")
                   (case request-method
                     (:get (push (list :edit attribute-name) navigation))
                     (t (setf status (list :error "Bad request")))))
                  (t (error "FSM error"))))))
              
           (finish-request ()
             (trivia:match status
               ((list :collection _)
                (ecase request-method
                  (:get (setf action :list))
                  (:post (setf action :add))))
               ((list :model _ _)
                (ecase request-method
                  (:get (setf action :view))
                  (:post (setf action :update))
                  (:patch (setf action :patch))
                  (:delete (setf action :delete))))
               ((list :attribute attribute)
                (ecase request-method
                  (:get (setf action :view))
                  (:post (setf action (list :update attribute))))))))
                           
      (loop for path-part in path
            do (route-request path-part))
      (when (not (eql status :end))
        (finish-request))
      (values (reverse navigation) action status))))

;; The request routing state machine
(defmethod route-request ((router rest-api-model-router) uri request-method)
  (let ((status :start)
        (path (split-sequence:split-sequence #\/ uri :remove-empty-subseqs t))
        (navigation nil)
        (model-package (model-package router))
        (action nil))
    
    (flet ((route-request (path-part)
             (trivia:match status
               (:start
                (aif (find-model-by-name path-part model-package :error-p nil)
                     (progn
                       (setf status (list :collection it))
                       (push (list :collection it) navigation))
                     (setf status (list :error :collection-not-found path-part))))
               ((list :collection model)
                ;; We assume it is an identifier of a model in the collection
                (push (list :find path-part) navigation)
                (setf status (list :model model path-part)))
               ((list :model model id)
                ;; We assume this is an attribute access
                (let ((attribute
                           (find path-part (model-attributes model)
                                 :key (alexandria:compose 'princ-to-string 'attribute-name)
                                 :test 'equalp)))
                     (if (null attribute)
                         (setf status (list :error :attribute-not-found path-part model))
                         (progn
                           (push (list :attribute (attribute-name attribute)) navigation)
                           (if (is-association? attribute)
                               ;; navigate to the associated model/collection
                               (if (is-multiple? attribute)
                                   (setf status (list :collection (find-model (association-type attribute) model-package)))
                                   ;; else
                                   (setf status (list :model (find-model (association-type attribute) model-package) nil)))
                               ;; else
                               (setf status (list :attribute (attribute-name attribute))))))))
               
               (t (error "FSM error"))))
              
           (finish-request ()
             (trivia:match status
               ((list :collection _)
                (ecase request-method
                  (:get (setf action :list))
                  (:post (setf action :add))))
               ((list :model _ _)
                (ecase request-method
                  (:get (setf action :view))
                  (:post (setf action :update))
                  (:patch (setf action :patch))
                  (:delete (setf action :delete))))
               ((list :attribute attribute)
                (ecase request-method
                  (:get (setf action :view))
                  (:post (setf action (list :update attribute))))))))
                           
      (loop for path-part in path
            do (route-request path-part))
      (when (not (eql status :end))
        (finish-request))
      (values (reverse navigation) action status))))

(defclass odata-router (model-web-router)
  ())

(defun parse-odata-url (url)
  "Use abnf and parser-combinators to parse OData urls")

;; http://docs.oasis-open.org/odata/odata/v4.0/errata03/os/complete/part2-url-conventions/odata-v4.0-errata03-os-part2-url-conventions-complete.html

(defmethod route-request ((router odata-router) uri request-method)
  )

(defgeneric navigate-to-model (implementation navigation action args))

(defmethod navigate-to-model ((impl (eql :standard)) navigation action args)
  (let (result)
    (loop for part in navigation
          do
             (trivia:match part
               ((list :collection model)
                (setf result model))
               ((list :find id)
                (setf result (find-model result id)))
               ((list :attribute attribute-name)
                (setf result (attribute-value result attribute-name)))))
    (trivia:match status
      ((list :attribute attribute-name)
       (ecase action
         (:update (setf (attribute-value result attribute-name)
			(first args))))))))


;; (abnf:parse-abnf-grammar
;;  (alexandria:read-file-into-string (asdf:system-relative-pathname
;;                                     :mold
;;                                     "odata.abnf"))
;;  "odataUri")
         
               
