;;;; mold.asd

(asdf:defsystem #:mold-examples-lms
  :description "Describe mold here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :mold-store :mold-postmodern :mold-web
	       :mold-webforms :mold-serializer
	       :hunchentoot :cl-who :cl-forms.who
		     :mold-basic-rest-api
		     :mold-webadmin)
  :components ((:module "examples/LMS"
			:components
			((:file "package")
			 (:file "model")
			 (:file "memory-database")
			 (:file "web")
			 (:file "rest-api")))))
