# Mold

Aspect Oriented Modeling and service generation for Common Lisp.

## Introduction

Object systems in general don't model relationships between objects. Best case, that information is kept in UML diagrams, or not at all.
But that loss of information prevents the generation of useful services by just looking at the model and its relationships.
So, those relationships should be kept explicit. That's what metamodeling languages do, for example, Java MOF.
The idea of this library is to provide a way to describe models, the relationships between them; and also describe different aspects of the model,
like persistence, serialization, validation, etc. and generate services from them, that connect the model to some library.


Some of the candidate service generators:

* Database
    - Postmodern DAO (extended with ORM features)
    - Mito
    - CLSQL
    - In-Memory DB
* Serialization
    - Schemata
* Validation
    - Schemata
* REST-API
    - OData like server
* UML
    - PlantUML generation
* Web forms
    - CL-FORMS
* User interface
    - Views
    - Editors
    - Breadcrumb navigation
    
## Design Overview

![design](design.png)
![design-specific](design-specific.png)

## Diagrams

```plantuml
@startuml
frame WeavedModel {
  [Model]
  [Model Aspect 1]
  [Model Aspect 2]
  [Model Aspect N]
}
WeavedModel ---> [Service Generator]

[Service Generator]

[Service]

[Service Generator] -> [Service]
@enduml
```

```plantuml
@startuml
frame DatabaseWeavedModel {
  [Model]
  [Database Aspect]
}

frame UMLWeavedModel {
  [Model']
  [UML Aspect]
}

frame SerializationWeavedModel {
  [Model'']
  [Serialization Aspect]
}

DatabaseWeavedModel -[hidden]-> UMLWeavedModel
UMLWeavedModel -[hidden]-> SerializationWeavedModel

DatabaseWeavedModel -> [Database ORM Mapper]
UMLWeavedModel -> [PlantUML generator]
SerializationWeavedModel -> [Serializer generator]

[Database ORM Mapper]
[PlantUML generator]

[Database ORM Mapper] -> [DAO classes]
[PlantUML generator] -> [PlantUML diagrams]
[Serializer generator] -> [JSON serialization]
@enduml
```

## License

Specify license here

