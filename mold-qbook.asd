(asdf:defsystem #:mold-qbook
  :description "QBook plugin that generates documentation for Mold models"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :mold-plantuml :qbook)
  :components ((:file "qbook")))
