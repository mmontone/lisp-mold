(defparameter *router* (make-instance 'mold/web:simple-model-web-router
                                      :model-package (find-model-package :lms)))

(route-request *router* "foo" :get)
(route-request *router* "course" :get)
(route-request *router* "academy" :get)
(route-request *router* "course" :post)
(route-request *router* "academy/instance" :get)
(route-request *router* "course/22" :get)
(route-request *router* "course/22" :post)
(route-request *router* "course/22/name" :get)
(route-request *router* "course/22/name" :post)
(route-request *router* "course/22/academy" :get)
(route-request *router* "course/22/academy/name" :get)
(route-request *router* "course/22/events" :get)
(route-request *router* "course/22/events/333" :get)
(route-request *router* "course/22/events/333" :post)
(route-request *router* "course/22/events" :post)
(route-request *router* "course/22/events/new" :get)
(route-request *router* "course/22/events/44/edit" :get)
(route-request *router* "" :get)

(defparameter *router* (make-instance 'mold/web:http-api-model-router
                                      :model-package (find-model-package :lms)))

(route-request *router* "foo" :get)
(route-request *router* "course" :get)
(route-request *router* "academy" :get)
(route-request *router* "course" :post)
(route-request *router* "academy/instance" :get)
(route-request *router* "course/22" :get)
(route-request *router* "course/22" :post)
(route-request *router* "course/22/name" :get)
(route-request *router* "course/22/name" :post)
(route-request *router* "course/22/academy" :get)
(route-request *router* "course/22/academy/name" :get)
(route-request *router* "course/22/events" :get)
(route-request *router* "course/22/events/333" :get)
(route-request *router* "course/22/events/333" :post)
(route-request *router* "course/22/events" :post)
(route-request *router* "course/22/events/new" :get)
(route-request *router* "course/22/events/44/edit" :get)
(route-request *router* "" :get)
