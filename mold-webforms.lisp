(defpackage :mold/webforms
  (:use :cl :forms :mold :anaphora)
  (:export :define-webform-from-model
   :define-webforms-from-package))

(in-package :mold/webforms)

(defun remove-from-plist-if (plist function)
  (loop for (key . rest) on plist by #'cddr
        do (assert rest () "Expected a proper plist, got ~S" plist)
        unless (funcall function key (first rest))
          collect key and collect (first rest)))

(defun form-field-options (options)

  ;; Idea for this:
  ;; (alexandria:flatten (mapcar 'c2mop:slot-definition-initargs
  ;;   (closer-mop:class-slots (find-class 'forms::choice-form-field))))
  ;; Also, we could use prefixes for distinguishing form field options, like: field-required, field-type, etc.
  (let ((field-options
          (remove-from-plist-if
           options
           (lambda (key val)
             (declare (ignore val))
             (member key '(:type :initarg :initform))))))
    (setf (getf field-options :accessor)
          `(quote ,(getf field-options :accessor)))
    field-options))

(defun form-field-type (attribute-type attribute)
  (aif (getf (attribute-options attribute)
             :form-type)
       it
       (trivia:match attribute-type
         ((cons 'mold::reference-to args)
          `(:choice
            :choices (lambda ()
                       (mold/store:contents (mold/store:get-collection ',(first args))))
            ))
         ((cons 'mold::set-of set-type)
          `(:choice
            :choices (lambda ()
                       (mold/store:contents (mold/store:get-collection ',(first set-type))))
            :multiple t))
         ((list 'or 'null type)
          (list* (form-field-type type attribute) :required-p nil))
         ((list 'or type 'null)
          (list* (form-field-type type attribute) :required-p nil))
	 ((cons 'member members)
	  `(:choice
	    :choices ',members))
	 ('boolean (list :boolean))
         ('string (list :string))
         ('integer (list :integer))
         ('datetime (list :datetime))
         ('list (list :string))
         (_ (error "Don't know how to transform to form field: ~a" attribute-type)))))

(defun form-field-from-attribute (attribute)
  (list* (attribute-name attribute)
         (append (form-field-type (attribute-type attribute) attribute)
                 (form-field-options (attribute-options attribute)))))

(defmacro define-webform-from-model (model-name &key ignore)
  (let* ((model (find-model model-name))
         (webform-aspect (find-model-aspect model-name :<<webform>>))
         (webform-model (if webform-aspect
                            (merge-models model webform-aspect)
                            model)))
    (let ((editable-attributes
            (remove-if (lambda (attr-name)
                         (member attr-name ignore))
                       (model-attributes webform-model)
                       :key 'attribute-name)))
      `(defform ,model-name ,(and webform-aspect (model-options webform-aspect))
         (,@(mapcar 'form-field-from-attribute editable-attributes)
          (submit :submit :label "Save"))))))

(defmacro define-webforms-from-package (package-name)
  `(progn
     ,@(loop for model in (list-model-package-models (find-model-package package-name))
             collect `(define-webform-from-model ,(model-name model)))))
