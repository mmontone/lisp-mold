;;;; mold.asd

(asdf:defsystem #:mold
  :description "Lisp Modelling Library"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (:alexandria :assoc-utils)
  :components ((:file "package")
               (:file "model")
               (:file "mold")))
