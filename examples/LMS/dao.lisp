(in-package :mold/lms)

(mold/postmodern:def-dao-from-model model)
(mold/postmodern:def-dao-from-model academy)
(mold/postmodern:def-orm-dao-from-model academy)

(mold/postmodern:def-orm-dao-from-model file)

(mold/postmodern:def-daos-from-package :lms)
(mold/postmodern:def-orm-daos-from-package :lms)
