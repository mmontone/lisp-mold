(in-package :mold/lms)

(defun start-rest-api ()
  (basic-rest-api:start-rest-api :lms))
