(in-package :mold/lms)

(def-classes-from-package :lms)

(defparameter *store*
  (make-instance 'mold/store:memory-model-store
		 :model-package (find-model-package :lms)))

(defparameter *my-course*
  (make-store-object *store*
                     (make-instance 'mold/lms::course
                                    :id 22
                                    :name "My course")))

(attribute-value *my-course* 'mold/lms::name)
(attribute-value *my-course* 'mold/lms::events)

(let ((events (attribute-value *my-course* 'mold/lms::events)))
  (collection-add events (make-instance 'mold/lms::event
                                        :id 22
                                        :subject "My event")))

(contents (attribute-value *my-course* 'mold/lms::events))
(collection-find (attribute-value *my-course* 'mold/lms::events)
                 22)

(defparameter *courses* (get-collection 'mold/lms::course))

(contents *courses*)

(collection-add *courses*
                (make-instance 'mold/lms::course
                               :id 22
                               :name "My course"))

(contents (get-collection 'mold/lms::course))

(get-class-model 'mold/lms::course)

(make-store-object *store* (make-instance 'mold/lms::course))
