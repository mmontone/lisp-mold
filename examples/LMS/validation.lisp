(in-package :mold/lms)

(def-model-aspect <<validation>> academy ()
  ((name :validator academy-name-validator)))
