(in-package :mold/lms)

(defun publish-qbook ()
  (let ((mold/qbook::*generate-uml* t))
    (qbook:publish-qbook
     (asdf:system-relative-pathname :academy "../src/mold.lisp")
     (make-instance 'qbook:html-generator
                    :title "Academy"
                    :output-directory (asdf:system-relative-pathname :academy "../doc/dev-manual/")
                    :highlight-syntax t))))
