(in-package :mold/lms)

(defun create-model-class-diagram (&optional (destination *standard-output*))
  (if (pathnamep destination)
      (with-open-file (stream destination
                              :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create
                              :external-format :utf8)
        (mold/plantuml:render-class-diagram stream :lms))
      (mold/plantuml:render-class-diagram destination :lms)))

(defparameter *plantuml-compiler* "plantuml")

(defun create-models-class-diagrams (folder &key (compile t))
  "Create a diagram for each model into FOLDER"

  (assert (uiop:directory-exists-p folder))

  (loop for model in (list-model-package-models (find-model-package :lms))
        for pathname := (merge-pathnames
                         (make-pathname :name (format nil "~a-CLASS-DIAGRAM" (model-name model))
                                        :type "puml")
                         folder)
        do
           (with-open-file (stream pathname
                                   :direction :output
                                   :if-exists :supersede
                                   :if-does-not-exist :create
                                   :external-format :utf8)
             (fmt:fmt stream "@startuml" #\newline #\newline)
             (mold/plantuml:render-model-class-diagram model :stream stream)
             (fmt:fmt stream #\newline "@enduml"))

           (when compile
             (let ((command (format nil "~a ~a"
                                    *plantuml-compiler*
                                    pathname)))
               (uiop/run-program:run-program command)))))

(create-model-class-diagram (asdf:system-relative-pathname :academy "../doc/class-diagram.puml"))

(create-models-class-diagrams (asdf:system-relative-pathname :academy "../doc/uml/"))

