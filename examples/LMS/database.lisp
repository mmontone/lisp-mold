(in-package :mold/lms)

(def-model-aspect <<database>> model ()
  ((id :db-type serial :primary-key t)
   (created :initform "now()" :db-default "now()"))
  (:table-name "public.users"))

(def-model-aspect <<database>> deletable ()
  ((deleted :db-type (or db-null timestamp))))

(def-model-aspect <<database>> academy ()
  ((name :unique t)
   (namespace :unique t)
   (picture :db-mapping (:one-to-one picture-id file)
            :nullable t
            :foreign-key (picture-id file))
   (settings :db-type json)
   (owner :db-mapping (:one-to-one owner-id user)
          :nullable nil
          :foreign-key (owner-id user))
   (users :db-mapping (:one-to-many academy-users academy-user academy-id))
   (activity :db-mapping nil)
   (courses :db-mapping (:one-to-many courses course academy-id)))
  (:table-name "public.academies"))

(def-model-aspect <<database>> user ()
  ((profile :db-type json :db-default "{}")
   (settings :db-type json :db-default "{}")
   (academies :db-mapping (:many-to-many academies academy academy-user))
   (user-academies :db-mapping (:one-to-many academy-users academy-user academy-id)))
  (:table-name "public.users"))

(def-model-aspect <<database>> file ()
  ((user :foreign-key (user-id user)))
  (:table-name "files"))

(def-model-aspect <<database>> academy-user ()
  ()
  (:table-name "public.academy-users"))
