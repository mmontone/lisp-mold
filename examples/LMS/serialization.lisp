(in-package :mold/lms)

(def-model-aspect <<serialization>> model ()
  ((id :serialize nil)))

(def-model-aspect <<serialization>> academy ()
  ((picture :serialize nil)))
