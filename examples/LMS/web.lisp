(in-package :mold/lms)

(in-model-package :lms)

(mold/store:connect-to-store
 'mold/store:memory-model-store
 :model-package (find-model-package :lms))

(mold/webforms:define-webforms-from-package :lms)

(defmethod mold/webadmin:present-model-object ((course course) model-object)
  (who:with-html-output-to-string (html)
    (:table
     (loop for attribute in (model-attributes (mold/store::model model-object))
           unless (is-association? attribute)
             do
                (who:htm (:tr
                          (:td (:b (who:str (attribute-name attribute))))
                          (:td (who:fmt "~a" (mold/store:attribute-value model-object (attribute-name attribute))))))))))

(defun start-web-admin ()
  (mold/webadmin:start-web-admin :lms))
