(in-package :mold/lms)

(def-model-package :lms)

(in-model-package :lms)

(def-model academy-model ()
  ((id :type integer :initarg :id :accessor id)
   (created :type datetime :initarg :created :accessor created))
  (:abstract t)
  (:print-object (model)
                 (print-unreadable-object (model stream)
                   (format stream "#~a ~a" (if (slot-boundp model 'id)
                                               (slot-value model 'id)
                                               "noid")
                           (type-of model)))))

(def-model deletable ()
  ((deleted :initarg :deleted
            :accessor deleted
            :type boolean
            :initform nil))
  (:abstract t))

(def-model academy (academy-model)
  ((name :type string :accessor name :initarg :name)
   (description :type string :accessor description :initarg :description)
   (short-description :type string :accessor short-description :initarg :short-description :initform "")
   (namespace :type string :accessor namespace :initarg :namespace)
   (picture :type (reference-to file 0 1) :accessor picture :initarg :picture)
   (email :initarg :email :type string :accessor email :initform "")
   (phone :initarg :phone
          :accessor phone
          :type string
          :initform "")
   (address :initarg :address
            :accessor address
            :type string
            :initform "")
   (website :initarg :website
            :accessor website
            :type string
            :initform "")
   (owner :type (reference-to user)
          :accessor owner)
   (subscription-id :initarg :subscription-id
                    :accessor subscription-id
                    :type (or null integer)
                    :initform nil)
   (settings :initarg :settings
             :accessor settings
             :type string
             :initform "{}")
   (users :type (set-of user 0 *) :accessor academy-users)
   (activity :type (set-of activity 0 *) :accessor activity)
   (courses :type (set-of course 0 *) :accessor courses)))

(def-model user (academy-model deletable)
  ((name :initarg :name
         :accessor name
         :type string)
   (email :initarg :email
          :accessor email
          :type string)
   (password :initarg :password
             :accessor password
             :type string)
   (profile :initarg :profile
            :type string)
   (settings :initarg :settings :type string)
   (academies :type (set-of academy))
   (user-academies :type (set-of academy-user))
   (participant-in :type (set-of event-participant))))

(def-model file (academy-model)
  ((filename :initarg :filename
             :type string
             :accessor filename)
   (mimetype :initarg :mimetype
             :accessor mimetype
             :type string)
   (repository :initarg :repository
               :accessor repository
               :type string
               :documentation "local directory, amazon s3, etc")
   (address :initarg :address
            :accessor address
            :type string
            :documentation "filepath, s3 url, etc")
   (user :type (reference-to user)
         :accessor user)))

(def-model academy-user ()
  ((roles :type list
          :accessor roles
          :initarg :roles)
   (user :type (reference-to user))
   (academy :type (reference-to academy)))
  (:documentation "The users that have access to the academy"))

(def-model event (academy-model)
  ((subject :type string
            :initarg :subject
            :accessor subject)
   (status :type string
           :initarg :status
           :initform "created"
           :accessor status)
   (content :type string
            :initarg :content
            :accessor content)
   (organizer :type (reference-to user))
   (creator :type (reference-to user))
   (event-type :initarg :event-type
               :accessor event-type
               :type (member :event :class))
   (starts :initarg :starts
           :type datetime
           :accessor starts)
   (ends :initarg :ends
         :type datetime
         :accessor ends)
   (course :type (reference-to course 0 1))
   (materials :type (set-of material))
   (participants :type (set-of event-participant)))
  (:id-attribute id))

(def-model course (academy-model)
  ((name :initarg :name
         :accessor name
         :type string)
   (description :initarg :description
                :accessor description
                :type string)
   (code :initarg :code
         :type string
         :accessor code)
   (status :initarg :status
           :type string
           :initform "unpublished"
           :accessor status)
   (starts :initarg :starts
           :type datetime
           :accessor starts)
   (ends :initarg :ends
         :accessor ends
         :type datetime)
   (academy :type (reference-to academy))
   (creator :type (reference-to user))
   (events :type (set-of event))
   (materials :type (set-of material)))
  (:id-attribute id)
  (:model-name name)
  (:print-model (course stream)
                (format stream (name course))))

(def-model material (academy-model)
  ((title :initarg :title
          :accessor title
          :type string)
   (description :initarg :description
                :accessor description
                :type string
                :initform "")
   (material-type :initarg :material-type
                  :accessor material-type
                  :type string
                  :documentation "file, document, folder, etc")
   (creator :type (reference-to user))
   (parent :type (reference-to material))
   (properties :initarg :properties
               :type list)
   (access-level :initarg :access-level
                 :accessor access-level
                 :type string
                 :initform "public")
   (file :type (reference-to file))
   (content-type :initarg :content-type
                 :accessor content-type
                 :type string)
   (content :initarg :content
            :accessor content
            :type string
            :initform "")))

(def-model event-participant ()
  ((user :type (reference-to user))
   (event :type (reference-to event))
   (enrollment :initarg :enrollment
               :accessor enrollment
               :type datetime)
   (role :initarg :role
         :accessor role
         :type string)))

(def-model notification (academy-model)
  ((user :type (reference-to user))
   (data :type list
         :initarg :data)
   (content :type string
            :accessor content
            :initarg :content)
   (html :type string
         :accessor html
         :initform "")
   (read :type (or null datetime)
         :accessor readp
         :initarg :read
         :initform nil)))

(def-model activity (academy-model)
  ((type :type string
         :initarg :type
         :accessor activity-type)
   (data :type list
         :initarg :data)
   (user :type (reference-to user))
   (course :type (reference-to course 0 1))
   (event :type (reference-to event 0 1))))
