;; TODO: review cl-containers for implementing collections
;; TODO: consider a iterate library extension for collection iteration

(defpackage :mold/store
  (:use :cl :mold)
  (:export
   :model-collection
   :*store*
   :with-store
   :get-collection
   :connect-to-store
   :model-store
   :memory-model-store
   :collection-add
   :collection-find
   :collection-query
   :collection-type
   :contents
   :collection-contents
   :make-store-object
   :store
   :attribute-value
   :store-object
   )
  (:documentation "Generic api for models storage"))

(in-package :mold/store)

(defclass model-store ()
  ((model-package :initarg :model-package
                  :accessor model-package
                  :initform (error "Provide :model-package"))))

(defclass store-object (pluggable-model-object)
  ((store :initarg :store
          :accessor store)))

(defclass model-collection (store-object)
  ((collection-type :initarg :collection-type
                    :accessor collection-type)))

(defmethod print-object ((collection model-collection) stream)
  (print-unreadable-object (collection stream :type t :identity t)
    (format stream "~a" (model-name (collection-type collection)))))

(defgeneric store-get-collection (store model-type))
(defgeneric store-save (store model))
(defgeneric initialize-store (store))
(defmethod initialize-store ((store model-store)))


(defvar *store*)

(defmacro with-store ((store-type &rest args) &body body)
  `(let ((*store* (make-instance ',store-type ,@args)))
     ,@body))

(defun connect-to-store (store-type &rest args)
  (let ((store (apply 'make-instance store-type args)))
    (initialize-store store)
    (setf *store* store)))

(defun get-collection (model-type &optional (store *store*))
  (store-get-collection store model-type))

(defun save (model &optional (store *store*))
  (store-save store model))

(defgeneric collection-find (collection id))
(defgeneric collection-query (collection query))
(defgeneric collection-add (collection object))

(defclass memory-model-store (model-store)
  ((objects :initform (make-hash-table)
            :accessor objects)
   (next-id :initform 0)))

(defmethod next-id ((store memory-model-store))
  (incf (slot-value store 'next-id)))

(defclass memory-model-collection (model-collection)
  ((contents :initarg :contents)
   (insert-function :initarg :insert-function
                    :accessor insert-function)))

(defun collection-contents (collection &rest args)
  (apply #'contents collection args))

(defgeneric contents (collection &rest args))

(defmethod contents ((collection memory-model-collection) &rest args)
  (funcall (slot-value collection 'contents)))

(defmethod store-get-collection ((store memory-model-store) model-type)
  (let ((model (find-model model-type (model-package store))))
    (make-instance 'memory-model-collection
                   :store store
                   :collection-type model
                   :contents (lambda ()
                               (mapcar (lambda (el)
                                         (make-store-object store el))
                                       (gethash model-type (objects store))))
                   :insert-function (lambda (obj)
                                      (push obj (gethash model-type (objects store)))))))

(defmethod collection-find ((collection memory-model-collection) id)
  (let ((id-slot (id-slot (collection-type collection))))
    (find id (contents collection)
          :key (lambda (x) (slot-value (mold::object x) id-slot)))))

(defmethod collection-add ((collection memory-model-collection) object)
  ;; Assign an id to the object first?
  (let ((id-slot (id-slot (collection-type collection))))
    (setf (slot-value object id-slot) (next-id (store collection))))
  (funcall (insert-function collection) object))

(defun make-store-object (store object &optional model)
  (make-instance 'store-object
                 :store store
                 :object object
                 :model (or model (get-class-model (class-name (class-of object))))))

(defmethod print-object ((store-object store-object) stream)
  (format stream "[~a]" (mold::object store-object)))

(defmethod attribute-value ((store-object store-object) attribute-name)
  (store-get-attribute-value
   (store store-object)
   store-object
   (find-attribute (model store-object) attribute-name)))

(defmethod attribute-boundp ((store-object store-object) attribute-name)
  (store-attribute-boundp
   (store store-object)
   store-object
   (find-attribute (model store-object) attribute-name)))

(defmethod store-get-attribute-value ((store memory-model-store) store-object attribute)
  (flet ((get-attribute-value ()
           (funcall (or (attribute-reader attribute)
                        (lambda (obj)
                          (slot-value obj (attribute-name attribute))))
                    (mold::object store-object))))
    (cond
      ((is-association? attribute)
       (if (is-multiple? attribute)
           ;; return a collection
           (make-instance 'memory-model-collection
                          :store store
                          :collection-type (find-model (second (attribute-type attribute)))
                          :contents (lambda ()
                                      (mapcar (lambda (el)
                                                (make-store-object store el))
                                              (get-attribute-value)))
                          :insert-function (lambda (obj)
                                             (when (not (slot-boundp (mold::object store-object)
                                                                     (attribute-name attribute)))
                                               (setf (slot-value (mold::object store-object)
                                                                 (attribute-name attribute))
                                                     nil))

                                             (push obj (slot-value (mold::object store-object)
                                                                   (attribute-name attribute)))))
           ;; just return the referenced object
           (get-attribute-value)))
      (t (get-attribute-value)))))
