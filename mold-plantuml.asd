;;;; mold.asd

(asdf:defsystem #:mold-plantuml
  :description "Describe mold here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (:mold :fmt :cl-json)
  :components ((:file "plantuml")))
